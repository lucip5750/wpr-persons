# WPR Persons
WPR Persons is a wordpress plugin built with [WP Reactive boilerplate](https://wp-reactive.com/).

## Setup and installation for front-end development
* **Install [Node 8.12.0 LTS or greater](https://nodejs.org)**
* **Install [Yarn](https://yarnpkg.com/en/docs/install)** (Or use npm if you prefer)

## Usage for front-end development
* Install required modules: `yarn` (or `npm install`)
* Build development version of app and watch for changes: `yarn build` (or `npm run build`)
* Build production version of app:`yarn prod` (or `npm run prod`)

## Setup and installation
* Install [Composer](https://getcomposer.org/)

## Usage
* Modify your wordpress ```composer.json```
	* add in the repositories element

		  ```
		 "repositories": [
					{
						"type": "vcs",
						"url": "https://lucip5750@bitbucket.org/lucip5750/wpr-persons.git"
					}
				],
		  ```
  
	* add in the require element

		  ```
		 "require": {
					"lucip5750/wpr-persons": "master"
				}
		  ```
  
* Run ```composer install```
* Go to administrition area > plugins and activate the WPR Persons plugin. This will add two persons for testing too.
* Go to administration area > Appearance > Widgets and add WPR Persons widget. It can be added as short code also using a plugin like [amr shortcode](https://wordpress.org/plugins/amr-shortcode-any-widget/). The short code can be added in any page or post content we want.


## Technologies

I used [WP Reactive boilerplate](https://wp-reactive.com/) to easily add ReactJS on plugins development. ReactJS added a big plus on web development, and I recommend it for its flexibility and for his professional community. Also, the ReactJS ecosystem has a lot of good libraries and a lot of good coding practices.
This plugin also offers an easy way to add REST Services, and we used it to get persons data from database in ```includes/Endpoint/Person.php```.

