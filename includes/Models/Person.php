<?php
namespace Pangolin\WPR\Models;


class Person 
{
    
    public function get(int $id)
    {
        $db = \WeDevs\ORM\Eloquent\Database::instance();
        $person = array();

        if ($person = $db->table('wpr_person')->find($id)) {
            $person->socialLinks =  $db->table('wpr_person_social_links')->where('person_id', '=', $person->id)->get('link');
        }

        return $person;
    }
}