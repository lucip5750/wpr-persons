<?php

/**
 * WPR-Persons
 *
 *
 * @package   WPR-Persons
 * @author    Lucian Padurariu
 */

namespace Pangolin\WPR\Endpoint;

use Pangolin\WPR;
use Pangolin\WPR\Models\Person as PersonModel;

/**
 * @subpackage REST_Controller
 */
class Person
{
    /**
     * Instance of this class.
     *
     * @since    0.8.1
     *
     * @var      object
     */
    protected static $instance = null;

    /**
     * Initialize the plugin by setting localization and loading public scripts
     * and styles.
     *
     * @since     0.8.1
     */
    private function __construct()
    {
        $plugin = WPR\Plugin::get_instance();
        $this->plugin_slug = $plugin->get_plugin_slug();
    }

    /**
     * Set up WordPress hooks and filters
     *
     * @return void
     */
    public function do_hooks()
    {
        add_action('rest_api_init', array($this, 'register_routes'));
    }

    /**
     * Return an instance of this class.
     *
     * @since     0.8.1
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
            self::$instance->do_hooks();
        }

        return self::$instance;
    }

    /**
     * Register the routes for the objects of the controller.
     */
    public function register_routes()
    {
        $version = '1';
        $namespace = $this->plugin_slug . '/v' . $version;
        $endpoint = '/person/';

        register_rest_route($namespace, $endpoint, array(
            array(
                'methods'               => \WP_REST_Server::READABLE,
                'callback'              => array($this, 'get_person'),
                'permission_callback'   => array($this, 'get_person_permissions_check'),
                'args'                  => array(
                    'id' => array(
                        'required' => true, // means that this parameter must be passed (whatever its value) in order for the request to succeed
                        'type' => 'integer',
                        'description' => 'The person id',
                        'validate_callback' => function ($param, $request, $key) {
                            return !empty($param);
                        } // prevent submission of empty field
                    ),
                ),
            ),
        ));
    }

    /**
     * Get Person
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|WP_REST_Request
     */
    public function get_person($request)
    {

        

        return new \WP_REST_Response(array(
            'success' => true,
            'value' => (new PersonModel())->get( (int)$request->get_param('id') )
        ), 200);
    }

    /**
     * Check if a given request has access
     *
     * @param WP_REST_Request $request Full data about the request.
     * @return WP_Error|bool
     */
    public function get_person_permissions_check($request)
    {
        return true;
    }
}
