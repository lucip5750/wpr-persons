<?php
/**
 * WPR-Persons
 *
 *
 * @package   WPR-Persons
 * @author    Lucian Padurariu
 */

namespace Pangolin\WPR;

/**
 * @subpackage Widget
 */
class Widget extends \WP_Widget {

	/**
	 * Initialize the widget
	 *
	 * @since 1.0.0
	 */
	public function __construct() {
		$plugin = Plugin::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();
		$this->version = $plugin->get_plugin_version();

		$widget_ops = array(
			'description' => esc_html__( 'WPR Persons widget.', $this->plugin_slug ),
		);

		parent::__construct( 'wpr-persons-widget', esc_html__( 'WPR Persons', $this->plugin_slug ), $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		wp_enqueue_script( $this->plugin_slug . '-widget-script', plugins_url( 'assets/js/widget.js', dirname( __FILE__ ) ), array( 'jquery' ), $this->version );
		wp_enqueue_style( $this->plugin_slug . '-widget-style', plugins_url( 'assets/css/widget.css', dirname( __FILE__ ) ), $this->version );

		$object_name = 'wpr_object_' . uniqid();

		$object = array(
			'person'       => $instance['person'],
			'api_nonce'   => wp_create_nonce( 'wp_rest' ),
			'api_url'	  => rest_url( $this->plugin_slug . '/v1/' ),
		);

		wp_localize_script( $this->plugin_slug . '-widget-script', $object_name, $object );

		echo $args['before_widget'];

		?><div class="wpr-persons-widget" data-object-id="<?php echo $object_name ?>"></div><?php

		echo $args['after_widget'];
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance
	 * @return string|void
	 */
	public function form( $instance ) {
		global $wpdb, $posts;
		$prefix = $wpdb->prefix;
		$queryResult = $wpdb->get_results("SELECT id, first_name, last_name FROM {$prefix}wpr_person");
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'person' ); ?>">
				<?php esc_html_e( 'Person name:', 'wpr-persons' ); ?>
			</label>
			<?php if($queryResult) { ?>
				<select id="<?php echo $this->get_field_id( 'person' ); ?>" name="<?php echo $this->get_field_name( 'person' ); ?>" >
				<option value="0">Select</option>
				<?php foreach ($queryResult as $row) { ?>
				  <option <?php echo $row->id == @$instance['person'] ? 'selected="selected"' : ''; ?> value="<?php echo esc_attr( $row->id ); ?>" ><?php 
					echo esc_attr( $row->first_name . ' ' . $row->last_name ); 
					?></option>;
				<?php } ?>
				</select>
			<?php } ?>
		</p>
		<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['person'] = sanitize_text_field( $new_instance['person'] );

		return $instance;
	}
}
