import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchWP from '../utils/fetchWP';
import PersonModal from '../components/PersonModal';
import PersonInfo from '../components/PersonInfo';

export default class Widget extends Component {

  constructor(props) {
    super(props);

    this.state = {
      person: null,
      modalIsOpen: false
    };

    this.fetchWP = new fetchWP({
      restURL: this.props.wpObject.api_url,
      restNonce: this.props.wpObject.api_nonce,
    });

    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

  }

  openModal() {
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  componentDidMount() {
    this.fetchWP.get('person?id=' + this.props.wpObject.person)
      .then(
        response => this.setState({ person: response.value }),
        (err) => console.log('error', err)
      );
  }

  render() {
    
    if (this.state.person) {
      let person = this.state.person;

      return (
        <div>
          <PersonInfo
            openModal={this.openModal}
            person={person}
          ></PersonInfo>
          <PersonModal
            isOpen={this.state.modalIsOpen}
            onAfterOpen={this.afterOpenModal}
            onRequestClose={this.closeModal}
            person={person}
          ></PersonModal>
        </div>
      );
    }
    else
      return (<div>Loading...</div>);
  }
}

Widget.propTypes = {
  wpObject: PropTypes.object
};