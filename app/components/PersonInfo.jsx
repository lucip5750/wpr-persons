import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class PersonInfo extends Component {

    render() {
        const { openModal, person } = this.props;

        return (
            <div style={{ border: '1px solid black' }} onClick={openModal}>
                <img src={`data:image/png;base64,${person.image}`} width="100px" height="100px"></img>
                <p>{person.first_name} {person.last_name}</p>
                <p>{person.position}</p>
            </div>
        );
    }
}

PersonInfo.propTypes = {
    openModal: PropTypes.func,
    person: PropTypes.object
}