import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-modal';

const customModalStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

Modal.setAppElement('.wpr-persons-widget')

export default class PersonModal extends Component {

    render() {
        const { person, isOpen, onAfterOpen, onRequestClose } = this.props;

        return (
            <Modal
                isOpen={isOpen}
                onAfterOpen={onAfterOpen}
                onRequestClose={onRequestClose}
                style={customModalStyles}
            >
                <p>{person.first_name} {person.last_name}</p>
                <p>{person.description}</p>
                <ul>
                    {person.socialLinks.map((value, index) => {
                        return <li key={index}>{value.link}</li>
                    })}
                </ul>
            </Modal>
        );
    }
}

PersonModal.propTypes = {
    isOpen: PropTypes.bool,
    onAfterOpen: PropTypes.func,
    onRequestClose: PropTypes.func,
    person: PropTypes.object
};